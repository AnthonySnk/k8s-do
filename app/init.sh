#!/bin/bash
set -e
# / Dont pass textplan here use gitlab variables /
# TOKEN_DIGITAL_OCEAN="67dbaaaa923f25dad45b1afd981f7c583e1b0a60445607f3e64b38c8f0e88dea"
# ECR_NAME="test-ecr"
# IMAGE_NAME="k8s_image"
echo "Updating System"
apt update
apt-get install -y \
apt-transport-https \
ca-certificates \
curl \
gnupg \
lsb-release \
software-properties-common

echo "Installing Docker"
apt install docker.io -y
docker --version
nohup dockerd >/dev/null 2>&1 & sleep 10

echo "Building Docker Image"
docker build -v /var/run/docker.sock:/var/run/docker.sock -t $IMAGE_NAME ./app/

echo "Installing Kubectl"
curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
chmod +x kubectl
mv kubectl /usr/local/bin
echo "Installing doctl"
wget https://github.com/digitalocean/doctl/releases/download/v1.60.0/doctl-1.60.0-linux-amd64.tar.gz
tar xf ./doctl-1.60.0-linux-amd64.tar.gz
mv ./doctl /usr/local/bin
doctl auth init --access-token $TOKEN_DIGITAL_OCEAN
echo "Creating Docker Image"
echo $(pwd) #/builds/AnthonySnk/k8s-do

docker login -u $TOKEN_DIGITAL_OCEAN -p $TOKEN_DIGITAL_OCEAN registry.digitalocean.com
docker tag $IMAGE_NAME registry.digitalocean.com/$ECR_NAME/k8s_image/$IMAGE_NAME
docker push registry.digitalocean.com/$ECR_NAME/$IMAGE_NAME