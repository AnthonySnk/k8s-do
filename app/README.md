- [What's this repo](#whats-this-repo)
- [Directories and Files](#directories-and-files)
- [k8s Folder](#k8s-folder)
- [scripts folder](#scripts-folder)
  - [setup file](#setup-file)
- [dockerignore file](#dockerignore-file)
- [bitbucket-pipelines.yml file](#bitbucket-pipelinesyml-file)
- [app files](#app-files)

# What's this repo

This is a CI/CD to auto-deploy an app using nodejs, bitbucket pipeline, docker, Kubernetes, aws ecr, aws eks.
The first that we have to know is how it works and what file does need to modify.

# Directories and Files

```txt
    | -- root
        | -- k8s
            | -- deploy.yaml
        | -- scripts
            | -- setup.sh
        .dockerignore
    bitbucket-pipelines.yml
    Dockerfile
    index.js
    package.json
    package-lock.json
    README.md
```

# k8s Folder

This folder contains the deploy.yaml, this file is no recommendable to modify is you are a developer, this file is to build your application on the Kubernetes cluster.
The most importante here is:

```yml
spec:
      containers:
      - name: my-node-app-ejam
        image: url-repo/my-first-repo:$BITBUCKET_COMMIT
        imagePullPolicy: Always
        env:
          - name: ENV
            value: $environment
```

Here we can change the image that we will use, we can set env vars, this means these variables going to live on the container to app uses it.

# scripts folder

This folder contains a file that bitbucket will use to install of dependency necessary to execute the deploy.yaml and connect with the cluster to the aws account.

## setup file

This file install some dependencies

| dependency        | reason                                |
| ----------------- | ------------------------------------- |
| get-text          | to use envsubst                       |
| AWS cli           | to use the command line of aws        |
| IAM authenticator | to connect with cluster               |
| kubectl           | to exec commands on kubernetes        |
| eks               | to use eksctl to get a cluster of aws |
| setup k8s conf    | setup cluster config                  |
| deploy app        | to deploy app on kubernetes           |

# dockerignore file

This file have the task to ignore folders that we do not upload to our repo

# bitbucket-pipelines.yml file

This file is used by bitbucket to do the task we want depends on branch we make a pull, push or merge.

# app files

index.js, package.json, package-lock.json.
This file for example for an app. Docker file builds an image with all files that the app will have.
package.json is important because this file says what dependencies we have to install to our app works.
