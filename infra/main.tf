/* k8s */
resource "digitalocean_kubernetes_cluster" "k8scluster" {
  name    = "${var.project_name}cluster"
  region  = var.digitalocean_region
  version = "1.21.2-do.2"

  node_pool {
    name       = "${var.project_name}pool"
    size       = "s-2vcpu-2gb"
    node_count = 1
  }
}
/* config kubectl  */
resource "local_file" "k8s_config" {
  content  = digitalocean_kubernetes_cluster.k8scluster.kube_config.0.raw_config
  filename = "kubeconfig.yaml"
}
provider "kubernetes" {
  host                   = digitalocean_kubernetes_cluster.k8scluster.endpoint
  config_path            = local_file.k8s_config.filename
  client_certificate     = base64decode(digitalocean_kubernetes_cluster.k8scluster.kube_config.0.client_certificate)
  client_key             = base64decode(digitalocean_kubernetes_cluster.k8scluster.kube_config.0.client_key)
  cluster_ca_certificate = base64decode(digitalocean_kubernetes_cluster.k8scluster.kube_config.0.cluster_ca_certificate)
}